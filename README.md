# ubuntu-release-upgrader-core

Manage release upgrades https://launchpad.net/ubuntu/+source/ubuntu-release-upgrader

## Official pages
* https://packages.ubuntu.com/en/ubuntu-release-upgrader-core
* https://git.launchpad.net/ubuntu/+source/ubuntu-release-upgrader/tree/COPYING

## Official documentation
* [*Upgrades*](https://help.ubuntu.com/community/Upgrades)
* [*Upgrading: do-release-upgrade*
  ](https://help.ubuntu.com/lts/serverguide/installing-upgrading.html)

## Unofficial documentation
* [*Ubuntu Linux:How to do unattended OS release upgrade?*
  ](https://www.linuxnix.com/ubuntu-linuxhow-to-do-unattended-os-release-upgrade/)
  2015 Surendra Anne
* [*Can I do a Silent or Unattended Release Upgrade?*
  ](https://askubuntu.com/questions/250733/can-i-do-a-silent-or-unattended-release-upgrade)
  2013-2018
* [*Ubuntu Release Upgrade – Fully automatic non-interactive upgrade*
  ](https://awaseroot.wordpress.com/2012/04/29/ubuntu-release-upgrade-fully-automatic-non-interactive-upgrade/)
  2012-04 Henri Siponen
* [*How do I upgrade to a newer version of Ubuntu?*
  ](https://askubuntu.com/questions/110477/how-do-i-upgrade-to-a-newer-version-of-ubuntu)
  2012

## 19.04 - 19.10
* [*How To Upgrade Ubuntu To 19.10 Eoan Ermine*
  ](https://linuxconfig.org/how-to-upgrade-ubuntu-to-19-10-eoan-ermine)
* [*How to Upgrade to Ubuntu 19.10 from 19.04 (Guide)*
  ](https://www.omgubuntu.co.uk/2019/10/how-to-upgrade-to-ubuntu-19-10)
* [*How to Upgrade to Ubuntu 19.10 From 19.04*
  ](https://www.debugpoint.com/2019/10/upgrade-ubuntu-19-10-from-19-04/)

## Use notes
### sudo do-release-upgrade
The following command has been enough on an up to date system:

$ sudo do-release-upgrade